object AviaWagesServer: TAviaWagesServer
  DisplayName = 'AviaWagesServer'
  OnStart = ServiceStart
  OnStop = ServiceStop
  Height = 590
  Width = 773
  object DSAppServer: TDSServer
    AutoStart = False
    Left = 44
    Top = 19
  end
  object DSTCPServerTransport: TDSTCPServerTransport
    Server = DSAppServer
    Filters = <>
    Left = 153
    Top = 19
  end
  object dsFireCasesClass: TDSServerClass
    OnGetClass = dsFireCasesClassGetClass
    Server = DSAppServer
    Left = 50
    Top = 96
  end
  object dsJumpsClass: TDSServerClass
    OnGetClass = dsJumpsClassGetClass
    Server = DSAppServer
    Left = 246
    Top = 96
  end
  object dsFightingsClass: TDSServerClass
    OnGetClass = dsFightingsClassGetClass
    Server = DSAppServer
    Left = 151
    Top = 96
  end
  object dsFlightsClass: TDSServerClass
    OnGetClass = dsFlightsClassGetClass
    Server = DSAppServer
    Left = 332
    Top = 96
  end
  object dsOtherPaysClass: TDSServerClass
    OnGetClass = dsOtherPaysClassGetClass
    Server = DSAppServer
    Left = 429
    Top = 96
  end
  object dsCommonPaysClass: TDSServerClass
    OnGetClass = dsCommonPaysClassGetClass
    Server = DSAppServer
    Left = 545
    Top = 96
  end
  object dsCalcPeriodClass: TDSServerClass
    OnGetClass = dsCalcPeriodClassGetClass
    Server = DSAppServer
    Left = 666
    Top = 96
  end
  object dsForestriesClass: TDSServerClass
    OnGetClass = dsForestriesClassGetClass
    Server = DSAppServer
    Left = 58
    Top = 248
  end
  object dsJumpKindsClass: TDSServerClass
    OnGetClass = dsJumpKindsClassGetClass
    Server = DSAppServer
    Left = 154
    Top = 248
  end
  object dsPlanesClass: TDSServerClass
    OnGetClass = dsPlanesClassGetClass
    Server = DSAppServer
    Left = 258
    Top = 248
  end
  object dsPayGroupsClass: TDSServerClass
    OnGetClass = dsPayGroupsClassGetClass
    Server = DSAppServer
    Left = 354
    Top = 248
  end
  object dsFlightTarifsClass: TDSServerClass
    OnGetClass = dsFlightTarifsClassGetClass
    Server = DSAppServer
    Left = 458
    Top = 248
  end
  object dsFightindsDetailClass: TDSServerClass
    OnGetClass = dsFightindsDetailClassGetClass
    Server = DSAppServer
    Left = 151
    Top = 168
  end
  object dsDepartmentsClass: TDSServerClass
    OnGetClass = dsDepartmentsClassGetClass
    Server = DSAppServer
    Left = 58
    Top = 336
  end
  object dsWorkersClass: TDSServerClass
    OnGetClass = dsWorkersClassGetClass
    Server = DSAppServer
    Left = 168
    Top = 336
  end
  object dsGroundsClass: TDSServerClass
    OnGetClass = dsGroundsClassGetClass
    Server = DSAppServer
    Left = 266
    Top = 336
  end
  object dsPaymentsClass: TDSServerClass
    OnGetClass = dsPaymentsClassGetClass
    Server = DSAppServer
    Left = 367
    Top = 336
  end
  object dsCatalogs: TDSServerClass
    OnGetClass = dsCatalogsGetClass
    Server = DSAppServer
    Left = 455
    Top = 336
  end
  object dsPayLinks: TDSServerClass
    OnGetClass = dsPayLinksGetClass
    Server = DSAppServer
    Left = 530
    Top = 335
  end
  object dsPayments2Payments: TDSServerClass
    OnGetClass = dsPayments2PaymentsGetClass
    Server = DSAppServer
    Left = 633
    Top = 335
  end
  object DSHTTPSrv: TDSHTTPService
    Server = DSAppServer
    Filters = <>
    Left = 270
    Top = 19
  end
  object dsUtils: TDSServerClass
    OnGetClass = dsUtilsGetClass
    Server = DSAppServer
    Left = 429
    Top = 19
  end
  object dsAccurals: TDSServerClass
    OnGetClass = dsAccuralsGetClass
    Server = DSAppServer
    Left = 501
    Top = 19
  end
  object dsFOT: TDSServerClass
    OnGetClass = dsFOTGetClass
    Server = DSAppServer
    Left = 581
    Top = 19
  end
  object dsReportParams: TDSServerClass
    OnGetClass = dsReportParamsGetClass
    Server = DSAppServer
    Left = 238
    Top = 440
  end
  object dsReportColumns: TDSServerClass
    OnGetClass = dsReportColumnsGetClass
    Server = DSAppServer
    Left = 135
    Top = 440
  end
  object dsReports: TDSServerClass
    OnGetClass = dsReportsGetClass
    Server = DSAppServer
    Left = 46
    Top = 440
  end
  object dsDedScale: TDSServerClass
    OnGetClass = dsDedScaleGetClass
    Server = DSAppServer
    Left = 350
    Top = 440
  end
end
