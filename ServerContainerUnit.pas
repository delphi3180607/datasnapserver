unit ServerContainerUnit;

interface

uses System.SysUtils, System.Classes, Vcl.SvcMgr,
  Datasnap.DSTCPServerTransport,
  Datasnap.DSHTTPCommon, Datasnap.DSHTTP,
  Datasnap.DSServer, Datasnap.DSCommonServer,
  IPPeerServer, IPPeerAPI, Datasnap.DSAuth,
  WinAPI.Windows;

type
  TAviaWagesServer = class(TService)
    DSAppServer: TDSServer;
    DSTCPServerTransport: TDSTCPServerTransport;
    dsFireCasesClass: TDSServerClass;
    dsJumpsClass: TDSServerClass;
    dsFightingsClass: TDSServerClass;
    dsFlightsClass: TDSServerClass;
    dsOtherPaysClass: TDSServerClass;
    dsCommonPaysClass: TDSServerClass;
    dsCalcPeriodClass: TDSServerClass;
    dsForestriesClass: TDSServerClass;
    dsJumpKindsClass: TDSServerClass;
    dsPlanesClass: TDSServerClass;
    dsPayGroupsClass: TDSServerClass;
    dsFlightTarifsClass: TDSServerClass;
    dsFightindsDetailClass: TDSServerClass;
    dsDepartmentsClass: TDSServerClass;
    dsWorkersClass: TDSServerClass;
    dsGroundsClass: TDSServerClass;
    dsPaymentsClass: TDSServerClass;
    dsCatalogs: TDSServerClass;
    dsPayLinks: TDSServerClass;
    dsPayments2Payments: TDSServerClass;
    DSHTTPSrv: TDSHTTPService;
    dsUtils: TDSServerClass;
    dsAccurals: TDSServerClass;
    dsFOT: TDSServerClass;
    dsReportParams: TDSServerClass;
    dsReportColumns: TDSServerClass;
    dsReports: TDSServerClass;
    dsDedScale: TDSServerClass;

    procedure ServiceStart(Sender: TService; var Started: Boolean);

    procedure dsFireCasesClassGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure dsJumpsClassGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure dsFightingsClassGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure dsFlightsClassGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure dsOtherPaysClassGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure dsCommonPaysClassGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure dsCalcPeriodClassGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure dsForestriesClassGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure dsJumpKindsClassGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure dsPlanesClassGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure dsPayGroupsClassGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure dsFlightTarifsClassGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure dsFightindsDetailClassGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure dsDepartmentsClassGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure dsWorkersClassGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure dsGroundsClassGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure dsPaymentsClassGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure dsCatalogsGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure dsPayLinksGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure dsPayments2PaymentsGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure ServiceStop(Sender: TService; var Stopped: Boolean);
    procedure dsUtilsGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure dsAccuralsGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure dsFOTGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure dsReportsGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure dsReportColumnsGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure dsReportParamsGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure dsDedScaleGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
  private
    { Private declarations }
  protected
    function DoStop: Boolean; override;
    function DoPause: Boolean; override;
    function DoContinue: Boolean; override;
    procedure DoInterrogate; override;
  public
    function GetServiceController: TServiceController; override;
  end;

var
  AviaWagesServer: TAviaWagesServer;

implementation

{$R *.dfm}

uses Connection, smFireCasesUnit, smCommonPaysUnit, smFightingsUnit, smFlightsUnit, smJumpsUnit, smOtherPaysUnit, smCalcPeriodUnit,
smFlightTarifsUnit, smPayGroupsUnit, smPlanesUnit, smJumpKindsUnit, smForestriesUnit, smFightingsDetailUnit,
smDepartmentsUnit, smWorkersUnit, smGroundsUnit, smPaymentsUnit, smCatalogsUnit, smPayLinksUnit, smPayments2PaymentsUnit,
smUtilsUnit, smAccuralsUnit, LoggerUnit, smFOTUnit, smReportsUnit, smReportParamsUnit, smReportColumnsUnit, smDedScaleUnit;

procedure TAviaWagesServer.dsAccuralsGetClass(DSServerClass: TDSServerClass;
  var PersistentClass: TPersistentClass);
begin
 PersistentClass := smAccuralsUnit.TsmAccurals;
end;

procedure TAviaWagesServer.dsCalcPeriodClassGetClass(DSServerClass: TDSServerClass; var PersistentClass: TPersistentClass);
begin
  PersistentClass := smCalcPeriodUnit.TsmCalcPeriod;
end;

procedure TAviaWagesServer.dsCatalogsGetClass(DSServerClass: TDSServerClass;
  var PersistentClass: TPersistentClass);
begin
  PersistentClass := smCatalogsUnit.TsmCatalogs;
end;

procedure TAviaWagesServer.dsCommonPaysClassGetClass(
  DSServerClass: TDSServerClass; var PersistentClass: TPersistentClass);
begin
  PersistentClass := smCommonPaysUnit.TsmCommonPays;
end;

procedure TAviaWagesServer.dsDedScaleGetClass(DSServerClass: TDSServerClass;
  var PersistentClass: TPersistentClass);
begin
  PersistentClass := smDedScaleUnit.TsmDedScale;
end;

procedure TAviaWagesServer.dsDepartmentsClassGetClass(
  DSServerClass: TDSServerClass; var PersistentClass: TPersistentClass);
begin
  PersistentClass := smDepartmentsUnit.TsmDepartments;
end;

procedure TAviaWagesServer.dsFightindsDetailClassGetClass(
  DSServerClass: TDSServerClass; var PersistentClass: TPersistentClass);
begin
  PersistentClass := smFightingsDetailUnit.TsmFightingsDetail;
end;

procedure TAviaWagesServer.dsFightingsClassGetClass(DSServerClass: TDSServerClass; var PersistentClass: TPersistentClass);
begin
  PersistentClass := smFightingsUnit.TsmFightings;
end;

procedure TAviaWagesServer.dsFireCasesClassGetClass(DSServerClass: TDSServerClass; var PersistentClass: TPersistentClass);
begin
  PersistentClass := smFireCasesUnit.TsmFireCases;
end;

procedure TAviaWagesServer.dsFlightsClassGetClass(DSServerClass: TDSServerClass;
  var PersistentClass: TPersistentClass);
begin
  PersistentClass := smFlightsUnit.TsmFlights;
end;

procedure TAviaWagesServer.dsFlightTarifsClassGetClass(
  DSServerClass: TDSServerClass; var PersistentClass: TPersistentClass);
begin
  PersistentClass := smFlightTarifsUnit.TsmFlightTarifs;
end;

procedure TAviaWagesServer.dsForestriesClassGetClass(
  DSServerClass: TDSServerClass; var PersistentClass: TPersistentClass);
begin
  PersistentClass := smForestriesUnit.TsmForestries;
end;

procedure TAviaWagesServer.dsFOTGetClass(DSServerClass: TDSServerClass;
  var PersistentClass: TPersistentClass);
begin
  PersistentClass := smFOTUnit.TsmFOT;
end;

procedure TAviaWagesServer.dsGroundsClassGetClass(DSServerClass: TDSServerClass;
  var PersistentClass: TPersistentClass);
begin
  PersistentClass := smGroundsUnit.TsmGrounds;
end;

procedure TAviaWagesServer.dsJumpKindsClassGetClass(
  DSServerClass: TDSServerClass; var PersistentClass: TPersistentClass);
begin
  PersistentClass := smJumpKindsUnit.TsmJumpKinds;
end;

procedure TAviaWagesServer.dsJumpsClassGetClass(DSServerClass: TDSServerClass; var PersistentClass: TPersistentClass);
begin
  PersistentClass := smJumpsUnit.TsmJumps;
end;

procedure TAviaWagesServer.dsOtherPaysClassGetClass(
  DSServerClass: TDSServerClass; var PersistentClass: TPersistentClass);
begin
  PersistentClass := smOtherPaysUnit.TsmOtherPays;
end;

procedure TAviaWagesServer.dsPayGroupsClassGetClass(
  DSServerClass: TDSServerClass; var PersistentClass: TPersistentClass);
begin
  PersistentClass := smPayGroupsUnit.TsmPayGroups;
end;

procedure TAviaWagesServer.dsPayLinksGetClass(DSServerClass: TDSServerClass;
  var PersistentClass: TPersistentClass);
begin
  PersistentClass := smPayLinksUnit.TsmPayLinks;
end;

procedure TAviaWagesServer.dsPayments2PaymentsGetClass(
  DSServerClass: TDSServerClass; var PersistentClass: TPersistentClass);
begin
  PersistentClass := smPayments2PaymentsUnit.TsmPayments2Payments;
end;

procedure TAviaWagesServer.dsPaymentsClassGetClass(
  DSServerClass: TDSServerClass; var PersistentClass: TPersistentClass);
begin
  PersistentClass := smPaymentsUnit.TsmPayments;
end;

procedure TAviaWagesServer.dsPlanesClassGetClass(DSServerClass: TDSServerClass;
  var PersistentClass: TPersistentClass);
begin
  PersistentClass := smPlanesUnit.TsmPlanes;
end;

procedure TAviaWagesServer.dsReportColumnsGetClass(
  DSServerClass: TDSServerClass; var PersistentClass: TPersistentClass);
begin
  PersistentClass := smReportColumnsUnit.TsmReportColumns;
end;

procedure TAviaWagesServer.dsReportParamsGetClass(DSServerClass: TDSServerClass;
  var PersistentClass: TPersistentClass);
begin
  PersistentClass := smReportParamsUnit.TsmReportParams;
end;

procedure TAviaWagesServer.dsReportsGetClass(DSServerClass: TDSServerClass;
  var PersistentClass: TPersistentClass);
begin
  PersistentClass := smReportsUnit.TsmReports;
end;

procedure TAviaWagesServer.dsUtilsGetClass(DSServerClass: TDSServerClass;
  var PersistentClass: TPersistentClass);
begin
  PersistentClass := smUtilsUnit.TsmUtils;
end;

procedure TAviaWagesServer.dsWorkersClassGetClass(DSServerClass: TDSServerClass;
  var PersistentClass: TPersistentClass);
begin
  PersistentClass := smWorkersUnit.TsmWorkers;
end;

procedure ServiceController(CtrlCode: DWord); stdcall;
begin
  AviaWagesServer.Controller(CtrlCode);
end;

function TAviaWagesServer.GetServiceController: TServiceController;
begin
  Result := ServiceController;
end;

function TAviaWagesServer.DoContinue: Boolean;
begin
  Result := inherited;
end;

procedure TAviaWagesServer.DoInterrogate;
begin
  inherited;
end;

function TAviaWagesServer.DoPause: Boolean;
begin
  DSAppServer.Stop;
  Logger.Add('������ ����������� (�����).');
  Result := inherited;
end;

function TAviaWagesServer.DoStop: Boolean;
begin
  DSAppServer.Stop;
  Result := inherited;
end;

procedure TAviaWagesServer.ServiceStart(Sender: TService; var Started: Boolean);
begin
  if dmConnection.Connect then
  begin
    DSAppServer.Start;
    Logger.Add('������ ��������.');
    DSHTTPSrv.Active := true;
    Logger.Add('HTTP ������ ������.');
  end else
  begin
    DSAppServer.Stop;
    Logger.Add('������ �����������.');
  end;
  Started := true;
end;

procedure TAviaWagesServer.ServiceStop(Sender: TService; var Stopped: Boolean);
begin
  DSAppServer.Stop;
  Logger.Add('������ �����������.');
  Stopped := true;
end;

end.

