object smUtils: TsmUtils
  Height = 349
  Width = 527
  object FDImport: TFDQuery
    Connection = dmConnection.connLocal
    SQL.Strings = (
      'call p_import_zp_data();')
    Left = 48
    Top = 48
  end
  object FDCalculate: TFDQuery
    Connection = dmConnection.connLocal
    SQL.Strings = (
      'call p_calculate(:nCalcPeriodId);')
    Left = 136
    Top = 48
    ParamData = <
      item
        Name = 'NCALCPERIODID'
        ParamType = ptInput
      end>
  end
  object FDCustomQuery: TFDQuery
    Connection = dmConnection.connLocal
    SQL.Strings = (
      'call p_calculate(:nCalcPeriodId);')
    Left = 232
    Top = 48
    ParamData = <
      item
        Name = 'NCALCPERIODID'
        ParamType = ptInput
      end>
  end
  object ParamTransform: TFMAParamTransform
    Left = 72
    Top = 128
  end
  object FDExport: TFDQuery
    Connection = dmConnection.connLocal
    SQL.Strings = (
      'call p_export_zp_data(:nCalPeriodId, :sWorkerId);')
    Left = 328
    Top = 48
    ParamData = <
      item
        Name = 'NCALPERIODID'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end
      item
        Name = 'SWORKERID'
        DataType = ftString
        ParamType = ptInput
        Value = Null
      end>
  end
end
