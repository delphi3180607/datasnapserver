inherited smWorkers: TsmWorkers
  inherited qrSelect: TFDQuery
    SQL.Strings = (
      'select * from v_workers /*wherefilter*/ /*limit*/')
  end
  object qrExtended: TFDQuery
    Connection = dmConnection.connLocal
    SQL.Strings = (
      'select w.*, '
      
        '(select sum(summa) from accurals a where a.worker_id = w.id and ' +
        'a.calc_period_id = :calc_period_id )::numeric(12,2) as summ_calc' +
        ' '
      'from v_workers w /*wherefilter*/ /*limit*/')
    Left = 184
    Top = 120
    ParamData = <
      item
        Name = 'CALC_PERIOD_ID'
        ParamType = ptInput
      end>
  end
end
