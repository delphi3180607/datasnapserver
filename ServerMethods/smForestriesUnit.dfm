inherited smForestries: TsmForestries
  inherited qrSelect: TFDQuery
    SQL.Strings = (
      'select * from v_forestries /*wherefilter*/')
  end
  inherited qrDelete: TFDQuery
    SQL.Strings = (
      'delete from forestries where id = :id;')
    ParamData = <
      item
        Name = 'ID'
        ParamType = ptInput
      end>
  end
  inherited qrUpdate: TFDQuery
    SQL.Strings = (
      'UPDATE public.forestries'
      'SET "name"= :name'
      'WHERE id= :id;')
    ParamData = <
      item
        Name = 'NAME'
        ParamType = ptInput
      end
      item
        Name = 'ID'
        ParamType = ptInput
      end>
  end
  inherited qrInsert: TFDQuery
    SQL.Strings = (
      'INSERT INTO public.forestries ("name")'
      'VALUES (:name) returning id;')
    ParamData = <
      item
        Name = 'NAME'
        ParamType = ptInput
        Value = Null
      end>
  end
end
