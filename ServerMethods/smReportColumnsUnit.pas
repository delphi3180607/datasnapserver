unit smReportColumnsUnit;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, smBaseUnit, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  FMAParamTransform, Data.DB, FireDAC.Comp.DataSet, FireDAC.Comp.Client;

type
  TsmReportColumns = class(TsmBase)
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  smReportColumns: TsmReportColumns;

implementation

{$R *.dfm}

end.
