unit smFireCasesUnit;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, smBaseUnit, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  Data.DB, FireDAC.Comp.DataSet, FireDAC.Comp.Client, FMAParamTransform, JSON;

{$METHODINFO ON}
type
  TsmFireCases = class(TsmBase)
    qrSelectEx: TFDQuery;
  private
    { Private declarations }
  public
     function GetListEx(filter: TJSONObject; out result_state: boolean; out errcode: string; out errmessage: string): TDataSet;
  end;
{$METHODINFO OFF}

var
  smFireCases: TsmFireCases;

implementation

{$R *.dfm}


function TsmFireCases.GetListEx(filter: TJSONObject; out result_state: boolean; out errcode: string; out errmessage: string): TDataSet;
begin
  ApplyContext(filter, qrSelectEx);
  if QueryOpen(qrSelectEx, errcode, errmessage) then
  begin
    result_state := true;
    result := qrSelectEx;
  end else
  begin
    result_state := false;
    result := nil;
  end;
end;


end.
