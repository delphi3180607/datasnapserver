unit smBaseUnit;

interface

uses
  System.SysUtils, System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Data.DB, StrUtils,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, JSON, Variants,
  FMAFunctions, Connection, FMAParamTransform, System.Generics.Collections;

{$METHODINFO ON}
type
  TsmBase = class(TDataModule)
    qrSelect: TFDQuery;
    qrDelete: TFDQuery;
    qrInsert: TFDQuery;
    qrUpdate: TFDQuery;
    ParamTransform: TFMAParamTransform;
    procedure DataModuleCreate(Sender: TObject);
  private
    FParamsInited: boolean;
  protected
    FInitialQueries: TDictionary<TFDQuery, string>;
    function QueryOpen(query: TFDQuery; out errcode: string; out errmessage: string): boolean;
    function QueryExec(query: TFDQuery; out errcode: string; out errmessage: string): boolean;
    procedure InitParams(var query: TFDQuery);
    procedure JSONToParams(source: TJSONObject; var dest: TFDQuery);
    procedure JSONToParam(source: TJSONObject; var dest: TFDQuery);
    procedure ApplyContext(context: TJSONObject; query: TFDQuery);
  public
    function GetList(filter: TJSONObject; out result_state: boolean; out errcode: string; out errmessage: string): TDataSet;
    function Insert(data: TJSONObject; out errcode: string; out errmessage: string): boolean;
    function InsertWithReturn(data: TJSONObject; out result_state: boolean; out errcode: string; out errmessage: string): TDataSet;
    function Update(data: TJSONObject; out errcode: string; out errmessage: string): boolean;
    function UpdateWithReturn(data: TJSONObject; out result_state: boolean; out errcode: string; out errmessage: string): TDataSet;
    function Delete(data: TJSONObject; out errcode: string; out errmessage: string): boolean;
  end;
{$METHODINFO OFF}

var
  smBase: TsmBase;

implementation

uses main, LoggerUnit;

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

function TsmBase.GetList(filter: TJSONObject; out result_state: boolean; out errcode: string; out errmessage: string): TDataSet;
begin
  ApplyContext(filter, qrSelect);
  if QueryOpen(qrSelect, errcode, errmessage) then
  begin
    result_state := true;
    result := qrSelect;
    if not FParamsInited then
    begin
      InitParams(qrDelete);
      InitParams(qrUpdate);
      InitParams(qrInsert);
      FParamsInited := true;
    end;
  end else
  begin
    result_state := false;
    result := nil;
  end;
end;

procedure TsmBase.ApplyContext(context: TJSONObject; query: TFDQuery);
var filter, params: TJSONObject; i: integer; pair: TJSONPair; pair_type, sqltext: string;
begin
  FInitialQueries.TryGetValue(query, sqltext);
  ParamTransform.Processed := false;
  if Assigned(context) then
  begin
    for i := 0 to context.Count-1 do
    begin
      pair := context.Pairs[i];
      pair_type := pair.JsonString.AsType<string>;
      if pair_type = 'pattern' then
      ParamTransform.ReplacePattern(sqltext, TJSONObject(pair.JsonValue));
    end;
  end;
  query.SQL.Text := sqltext;
  if Assigned(context) then
  begin
    for i := 0 to context.Count-1 do
    begin
      pair := context.Pairs[i];
      pair_type := pair.JsonString.AsType<string>;
      if pair_type = 'parameter' then
      JSONToParam(TJSONObject(pair.JsonValue),query);
    end;
  end;
end;

procedure TsmBase.InitParams(var query: TFDQuery);
var i: integer; p: TFDParam;
begin
  for i := 0 to qrSelect.Fields.Count-1 do
  begin
    p := query.Params.FindParam(qrSelect.Fields[i].FieldName);
    if Assigned(p) then
    begin
      p.DataType := qrSelect.Fields[i].DataType;
    end;
  end;
end;

function TsmBase.Insert(data: TJSONObject; out errcode: string; out errmessage: string): boolean;
begin
  JsonToParams(data, qrInsert);
  result := QueryExec(qrInsert, errcode, errmessage);
end;

function TsmBase.InsertWithReturn(data: TJSONObject; out result_state: boolean; out errcode: string; out errmessage: string): TDataSet;
var id: Variant; errcode1, errmessage1: string;
begin
  result_state := true;
  result := nil;
  JsonToParams(data, qrInsert);
  if QueryOpen(qrInsert, errcode, errmessage) then
  begin
    result_state := true;
    id := qrInsert.Fields[0].Value;
  end else
  begin
    result_state := false;
    id := null;
  end;
  ParamTransform.ResetContext;
  ParamTransform.AddPatternContext('/*wherefilter*/', 'id = :id');
  ParamTransform.AddParameterContext('id', qrSelect.FieldByName('id').DataType, id);
  ApplyContext(ParamTransform.Context, qrSelect);
  if QueryOpen(qrSelect, errcode1, errmessage1) then
  begin
    result := qrSelect;
  end else
  begin
    result_state := false;
    errcode := errcode1;
    errmessage := errmessage1;
    result := nil;
  end;
end;

function TsmBase.Delete(data: TJSONObject; out errcode: string; out errmessage: string): boolean;
begin
  JsonToParams(data, qrDelete);
  result := QueryExec(qrDelete, errcode, errmessage);
end;

function TsmBase.Update(data: TJSONObject; out errcode: string; out errmessage: string): boolean;
begin
  JsonToParams(data, qrUpdate);
  result := QueryExec(qrUpdate, errcode, errmessage);
end;

function TsmBase.UpdateWithReturn(data: TJSONObject; out result_state: boolean; out errcode: string; out errmessage: string): TDataSet;
var id: Variant; errcode1, errmessage1: string;
begin
  result_state := true;
  result := nil;
  JsonToParams(data, qrUpdate);
  result_state := QueryExec(qrUpdate, errcode, errmessage);
  id := qrUpdate.Params.ParamByName('id').Value;
  ParamTransform.ResetContext;
  ParamTransform.AddPatternContext('/*wherefilter*/', 'id = :id');
  ParamTransform.AddParameterContext('id', qrUpdate.Params.ParamByName('id').DataType, id);
  ApplyContext(ParamTransform.Context, qrSelect);
  if QueryOpen(qrSelect, errcode1, errmessage1) then
  begin
    result := qrSelect;
  end else
  begin
    result_state := false;
    errcode := errcode1;
    errmessage := errmessage1;
    result := nil;
  end;
end;

procedure TsmBase.DataModuleCreate(Sender: TObject);
var i: integer;
begin
 FParamsInited := false;
 FInitialQueries := TDictionary<TFDQuery, string>.Create;
 for i := 0 to self.ComponentCount-1 do
 begin
  if self.Components[i] is TFDQuery then
  FInitialQueries.Add(TFDQuery(self.Components[i]), TFDQuery(self.Components[i]).SQL.Text);
 end;
end;

procedure TsmBase.JSONToParam(source: TJSONObject; var dest: TFDQuery);
var p: TJSONPair; field_name: string; param: TFDParam;
begin
  p := source.Pairs[0];
  field_name := p.JsonString.AsType<string>;
  param := dest.Params.FindParam(field_name);
  if Assigned(param) then
  begin
     param.DataType := ParamTransform.JSONToDataType(TJSONObject(p.JsonValue));
     param.Value := ParamTransform.JSONToVariant(TJSONObject(p.JsonValue));
  end;
end;

procedure TsmBase.JSONToParams(source: TJSONObject; var dest: TFDQuery);
var i: integer; p: TJSONPair; field_name: string; param: TFDParam;
begin
  for i := 0 to source.Count-1 do
  begin
    p := source.Pairs[i];
    field_name := p.JsonString.AsType<string>;
    param := dest.Params.FindParam(field_name);
    if Assigned(param) then
    begin
      try
        param.Value := ParamTransform.JSONToVariant(TJSONObject(p.JsonValue));
      except
        on e: Exception do
        begin
          Logger.Add('������ ���������� ���������:');
          Logger.Add(E.Message);
        end;
      end;
    end;
  end;
end;

function TsmBase.QueryExec(query: TFDQuery; out errcode: string; out errmessage: string): boolean;
begin
  result := true;
  try
    query.ExecSQL;
  except
    on e: Exception do
    begin
      Logger.Add(E.Message);
      Logger.Add(': '+query.SQL.Text);
      errmessage := E.Message;
      result := false;
    end;
  end;
end;

function TsmBase.QueryOpen(query: TFDQuery; out errcode: string; out errmessage: string): boolean;
begin
  result := true;
  try
    query.Close;
    query.Open;
  except
    on e: Exception do
    begin
      Logger.Add(E.Message);
      Logger.Add(': '+query.SQL.Text);
      errmessage := E.Message;
      result := false;
    end;
  end;
end;

end.
