inherited smPayments2Payments: TsmPayments2Payments
  inherited qrSelect: TFDQuery
    SQL.Strings = (
      'select * from v_payments2payments /*wherefilter*/')
  end
  inherited qrDelete: TFDQuery
    SQL.Strings = (
      'delete from payments2payments where id = :id;')
    ParamData = <
      item
        Name = 'ID'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end>
  end
  inherited qrUpdate: TFDQuery
    SQL.Strings = (
      'UPDATE public.payments2payments'
      
        'SET parent_payment_id = :parent_payment_id, payment_id = :paymen' +
        't_id'
      'WHERE id= :id;')
    ParamData = <
      item
        Name = 'PARENT_PAYMENT_ID'
        ParamType = ptInput
      end
      item
        Name = 'PAYMENT_ID'
        ParamType = ptInput
      end
      item
        Name = 'ID'
        ParamType = ptInput
      end>
  end
  inherited qrInsert: TFDQuery
    SQL.Strings = (
      'INSERT INTO public.payments2payments'
      '(parent_payment_id, payment_id)'
      'VALUES(:parent_payment_id, :payment_id)'
      'returning id;')
    ParamData = <
      item
        Name = 'PARENT_PAYMENT_ID'
        ParamType = ptInput
      end
      item
        Name = 'PAYMENT_ID'
        ParamType = ptInput
      end>
  end
end
