unit smUtilsUnit;

interface

uses
  System.SysUtils, System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, JSON, Connection, FMAParamTransform;

{$METHODINFO ON}
type
  TsmUtils = class(TDataModule)
    FDImport: TFDQuery;
    FDCalculate: TFDQuery;
    FDCustomQuery: TFDQuery;
    ParamTransform: TFMAParamTransform;
    FDExport: TFDQuery;
  private
    procedure ApplyContext(query:TFDQuery; context: TJSONObject);
    procedure JSONToParam(source: TJSONObject; var dest: TFDQuery);
    procedure QueryOpen(query: TFDQuery);
  public
    function ImportZPData: boolean;
    function ExportZpData(ncalcperiodid: integer; sworkerid: string): boolean;
    function Calculate(calc_period_id: integer): boolean;
    function OpenCustomQuery(filter: TJSONObject): TDataSet;
  end;
{$METHODINFO OFF}

var
  smUtils: TsmUtils;

implementation

uses LoggerUnit;

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

procedure TsmUtils.ApplyContext(query:TFDQuery; context: TJSONObject);
var filter, params: TJSONObject; i: integer; pair: TJSONPair; pair_type, sqltext: string;
begin
  if Assigned(context) then
  begin
    for i := 0 to context.Count-1 do
    begin
      pair := context.Pairs[i];
      pair_type := pair.JsonString.AsType<string>;
      if pair_type = 'sqltext' then
      ParamTransform.SetText(sqltext, TJSONObject(pair.JsonValue));
      query.SQL.Text := sqltext;
    end;
    for i := 0 to context.Count-1 do
    begin
      pair := context.Pairs[i];
      pair_type := pair.JsonString.AsType<string>;
      if pair_type = 'parameter' then
      JSONToParam(TJSONObject(pair.JsonValue),query);
    end;
  end;
end;

procedure TsmUtils.JSONToParam(source: TJSONObject; var dest: TFDQuery);
var p: TJSONPair; field_name: string; param: TFDParam;
begin
  p := source.Pairs[0];
  field_name := p.JsonString.AsType<string>;
  param := dest.Params.FindParam(field_name);
  if Assigned(param) then
  begin
     param.DataType := ParamTransform.JSONToDataType(TJSONObject(p.JsonValue));
     param.Value := ParamTransform.JSONToVariant(TJSONObject(p.JsonValue));
  end;
end;


function TsmUtils.Calculate(calc_period_id: integer): boolean;
begin
  try
    FDCalculate.Params[0].Value := calc_period_id;
    FDCalculate.Execute;
  except
    on E: Exception do
    begin
      Logger.Add('������ ��� ��� ������� ����������.');
      Logger.Add(E.Message);
      result := false;
      exit
    end;
  end;
  result := true;
end;

function TsmUtils.ExportZpData(ncalcperiodid: integer; sworkerid: string): boolean;
begin
  try
    FDExport.Params.ParamByName('nCalPeriodId').Value := ncalcperiodid;
    FDExport.Params.ParamByName('sWorkerId').Value := sWorkerId;
    FDExport.Execute;
  except
    on E: Exception do
    begin
      Logger.Add('������ ��� �������� ������ � �� �����.');
      Logger.Add(E.Message);
      result := false;
      exit
    end;
  end;
  result := true;
end;

function TsmUtils.OpenCustomQuery(filter: TJSONObject): TDataSet;
begin
  ApplyContext(FDCustomQuery, filter);
  QueryOpen(FDCustomQuery);
  result := FDCustomQuery;
end;

function TsmUtils.ImportZPData: boolean;
begin
  try
    FDImport.Execute;
  except
    on E: Exception do
    begin
      Logger.Add('������ ��� ���������� ������ �� �� �����.');
      Logger.Add(E.Message);
      result := false;
      exit
    end;
  end;
  result := true;
end;


procedure TsmUtils.QueryOpen(query: TFDQuery);
begin
  try
    query.Close;
    query.Open;
  except
    on e: Exception do
    begin
      Logger.Add('������ ���������� ������� ������-');
      Logger.Add(E.Message);
      Logger.Add(': '+query.SQL.Text);
    end;
  end;
end;


end.
