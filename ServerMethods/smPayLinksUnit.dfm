inherited smPayLinks: TsmPayLinks
  inherited qrSelect: TFDQuery
    SQL.Strings = (
      'select * from v_paylinks /*wherefilter*/')
  end
  inherited qrUpdate: TFDQuery
    SQL.Strings = (
      'UPDATE public.paylinks'
      'SET pay_fire_id= :pay_fire_id, '
      'pay_jumps_id= :pay_jumps_id, '
      'pay_flights_id= :pay_flights_id, '
      'pay_over_id= :pay_over_id, '
      'pay_holiday_id= :pay_holiday_id, '
      'firecalc= :firecalc'
      'where id = :id;')
    ParamData = <
      item
        Name = 'PAY_FIRE_ID'
        ParamType = ptInput
      end
      item
        Name = 'PAY_JUMPS_ID'
        ParamType = ptInput
      end
      item
        Name = 'PAY_FLIGHTS_ID'
        ParamType = ptInput
      end
      item
        Name = 'PAY_OVER_ID'
        ParamType = ptInput
      end
      item
        Name = 'PAY_HOLIDAY_ID'
        ParamType = ptInput
      end
      item
        Name = 'FIRECALC'
        ParamType = ptInput
      end
      item
        Name = 'ID'
        ParamType = ptInput
      end>
  end
end
