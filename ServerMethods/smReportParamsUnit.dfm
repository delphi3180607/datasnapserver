inherited smReportParams: TsmReportParams
  inherited qrSelect: TFDQuery
    SQL.Strings = (
      'select * from v_report_params /*wherefilter*/ ')
  end
  inherited qrDelete: TFDQuery
    SQL.Strings = (
      'delete from report_params where id = :id; ')
    ParamData = <
      item
        Name = 'ID'
        ParamType = ptInput
      end>
  end
  inherited qrUpdate: TFDQuery
    SQL.Strings = (
      'UPDATE public.report_params'
      'SET report_id=:report_id, code=:code, caption=:caption, '
      'param_type=:param_type, param_length=:param_length, '
      'param_decimals=:param_decimals, getlist_method=:getlist_method, '
      
        'display_field_name=:display_field_name, key_field_name=:key_fiel' +
        'd_name, '
      'order_num = :order_num,'
      'master_param_id = :master_param_id, '
      'filter_expression = :filter_expression '
      'WHERE id=:id;')
    ParamData = <
      item
        Name = 'REPORT_ID'
        ParamType = ptInput
      end
      item
        Name = 'CODE'
        ParamType = ptInput
      end
      item
        Name = 'CAPTION'
        ParamType = ptInput
      end
      item
        Name = 'PARAM_TYPE'
        ParamType = ptInput
      end
      item
        Name = 'PARAM_LENGTH'
        ParamType = ptInput
      end
      item
        Name = 'PARAM_DECIMALS'
        ParamType = ptInput
      end
      item
        Name = 'GETLIST_METHOD'
        ParamType = ptInput
      end
      item
        Name = 'DISPLAY_FIELD_NAME'
        ParamType = ptInput
      end
      item
        Name = 'KEY_FIELD_NAME'
        ParamType = ptInput
      end
      item
        Name = 'ORDER_NUM'
        ParamType = ptInput
      end
      item
        Name = 'MASTER_PARAM_ID'
        ParamType = ptInput
      end
      item
        Name = 'FILTER_EXPRESSION'
        ParamType = ptInput
      end
      item
        Name = 'ID'
        ParamType = ptInput
      end>
  end
  inherited qrInsert: TFDQuery
    SQL.Strings = (
      'INSERT INTO public.report_params'
      
        '(report_id, code, caption, param_type, param_length, param_decim' +
        'als, getlist_method, display_field_name, key_field_name, order_n' +
        'um,'
      'master_param_id, filter_expression)'
      'VALUES'
      
        '(:report_id, :code, :caption, :param_type, :param_length, :param' +
        '_decimals, :getlist_method, :display_field_name, :key_field_name' +
        ', :order_num,'
      ':master_param_id, :filter_expression)'
      'returning id;')
    ParamData = <
      item
        Name = 'REPORT_ID'
        ParamType = ptInput
      end
      item
        Name = 'CODE'
        ParamType = ptInput
      end
      item
        Name = 'CAPTION'
        ParamType = ptInput
      end
      item
        Name = 'PARAM_TYPE'
        ParamType = ptInput
      end
      item
        Name = 'PARAM_LENGTH'
        ParamType = ptInput
      end
      item
        Name = 'PARAM_DECIMALS'
        ParamType = ptInput
      end
      item
        Name = 'GETLIST_METHOD'
        ParamType = ptInput
      end
      item
        Name = 'DISPLAY_FIELD_NAME'
        ParamType = ptInput
      end
      item
        Name = 'KEY_FIELD_NAME'
        ParamType = ptInput
      end
      item
        Name = 'ORDER_NUM'
        ParamType = ptInput
      end
      item
        Name = 'MASTER_PARAM_ID'
        ParamType = ptInput
      end
      item
        Name = 'FILTER_EXPRESSION'
        ParamType = ptInput
      end>
  end
end
