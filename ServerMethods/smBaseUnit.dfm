object smBase: TsmBase
  OnCreate = DataModuleCreate
  Height = 244
  Width = 445
  object qrSelect: TFDQuery
    Connection = dmConnection.connLocal
    Left = 56
    Top = 32
  end
  object qrDelete: TFDQuery
    Connection = dmConnection.connLocal
    Left = 136
    Top = 33
  end
  object qrUpdate: TFDQuery
    Connection = dmConnection.connLocal
    Left = 304
    Top = 32
  end
  object qrInsert: TFDQuery
    Connection = dmConnection.connLocal
    Left = 216
    Top = 32
  end
  object ParamTransform: TFMAParamTransform
    Left = 72
    Top = 128
  end
end
