inherited smPayGroups: TsmPayGroups
  inherited qrSelect: TFDQuery
    SQL.Strings = (
      'select * from v_paygroups /*wherefilter*/')
  end
  inherited qrDelete: TFDQuery
    SQL.Strings = (
      'delete from paygroups where id = :id;')
    ParamData = <
      item
        Name = 'ID'
        ParamType = ptInput
      end>
  end
  inherited qrUpdate: TFDQuery
    SQL.Strings = (
      'UPDATE public.paygroups'
      'SET code= :code, name= :name'
      'WHERE id= :id;')
    ParamData = <
      item
        Name = 'CODE'
        ParamType = ptInput
      end
      item
        Name = 'NAME'
        ParamType = ptInput
      end
      item
        Name = 'ID'
        ParamType = ptInput
      end>
  end
  inherited qrInsert: TFDQuery
    SQL.Strings = (
      'INSERT INTO public.paygroups (code, name)'
      'VALUES(:code, :name) returning id;')
    ParamData = <
      item
        Name = 'CODE'
        ParamType = ptInput
      end
      item
        Name = 'NAME'
        ParamType = ptInput
      end>
  end
end
