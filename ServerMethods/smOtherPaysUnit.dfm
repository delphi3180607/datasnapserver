inherited smOtherPays: TsmOtherPays
  inherited qrSelect: TFDQuery
    SQL.Strings = (
      'select * from v_other_pays /*wherefilter*/')
  end
  inherited qrDelete: TFDQuery
    SQL.Strings = (
      'delete from other_pays where id = :id;')
    ParamData = <
      item
        Name = 'ID'
        ParamType = ptInput
      end>
  end
  inherited qrUpdate: TFDQuery
    SQL.Strings = (
      'UPDATE public.other_pays'
      
        'SET fire_case_id= :fire_case_id, payment_id= :payment_id, worker' +
        '_id= :worker_id, '
      
        'calc_period_id= :calc_period_id, hours= :hours, percent= :percen' +
        't, isholiday= :isholiday, '
      'summa= :summa, isstorno = :isstorno'
      'WHERE id= :id;')
    ParamData = <
      item
        Name = 'FIRE_CASE_ID'
        ParamType = ptInput
      end
      item
        Name = 'PAYMENT_ID'
        ParamType = ptInput
      end
      item
        Name = 'WORKER_ID'
        ParamType = ptInput
      end
      item
        Name = 'CALC_PERIOD_ID'
        ParamType = ptInput
      end
      item
        Name = 'HOURS'
        ParamType = ptInput
      end
      item
        Name = 'PERCENT'
        ParamType = ptInput
      end
      item
        Name = 'ISHOLIDAY'
        ParamType = ptInput
      end
      item
        Name = 'SUMMA'
        ParamType = ptInput
      end
      item
        Name = 'ISSTORNO'
        ParamType = ptInput
      end
      item
        Name = 'ID'
        ParamType = ptInput
      end>
  end
  inherited qrInsert: TFDQuery
    SQL.Strings = (
      'INSERT INTO public.other_pays'
      
        '(fire_case_id, payment_id, worker_id, calc_period_id, hours, "pe' +
        'rcent", isholiday, summa, isstorno)'
      
        'VALUES(:fire_case_id, :payment_id, :worker_id, :calc_period_id, ' +
        ':hours, :percent, :isholiday, :summa, :isstorno)'
      'returning id;')
    ParamData = <
      item
        Name = 'FIRE_CASE_ID'
        ParamType = ptInput
      end
      item
        Name = 'PAYMENT_ID'
        ParamType = ptInput
      end
      item
        Name = 'WORKER_ID'
        ParamType = ptInput
      end
      item
        Name = 'CALC_PERIOD_ID'
        ParamType = ptInput
      end
      item
        Name = 'HOURS'
        ParamType = ptInput
      end
      item
        Name = 'PERCENT'
        ParamType = ptInput
      end
      item
        Name = 'ISHOLIDAY'
        ParamType = ptInput
      end
      item
        Name = 'SUMMA'
        ParamType = ptInput
      end
      item
        Name = 'ISSTORNO'
        ParamType = ptInput
      end>
  end
end
