inherited smFightings: TsmFightings
  inherited qrSelect: TFDQuery
    SQL.Strings = (
      'select * from v_fightings /*wherefilter*/')
  end
  inherited qrDelete: TFDQuery
    SQL.Strings = (
      'delete from public.fightings where id = :id;')
    ParamData = <
      item
        Name = 'ID'
        ParamType = ptInput
      end>
  end
  inherited qrUpdate: TFDQuery
    SQL.Strings = (
      'UPDATE public.fightings'
      
        'SET fire_case_id= :fire_case_id, worker_id= :worker_id, calc_per' +
        'iod_id= :calc_period_id, '
      'date_from= :date_from, date_to= :date_to, isstorno = :isstorno'
      'WHERE id= :id;')
    ParamData = <
      item
        Name = 'FIRE_CASE_ID'
        ParamType = ptInput
      end
      item
        Name = 'WORKER_ID'
        ParamType = ptInput
      end
      item
        Name = 'CALC_PERIOD_ID'
        ParamType = ptInput
      end
      item
        Name = 'DATE_FROM'
        ParamType = ptInput
      end
      item
        Name = 'DATE_TO'
        ParamType = ptInput
      end
      item
        Name = 'ISSTORNO'
        ParamType = ptInput
      end
      item
        Name = 'ID'
        ParamType = ptInput
      end>
  end
  inherited qrInsert: TFDQuery
    SQL.Strings = (
      'INSERT INTO public.fightings'
      
        '(fire_case_id, worker_id, calc_period_id, date_from, date_to, is' +
        'storno)'
      
        'VALUES(:fire_case_id, :worker_id, :calc_period_id, :date_from, :' +
        'date_to, :isstorno)'
      'returning id;')
    ParamData = <
      item
        Name = 'FIRE_CASE_ID'
        ParamType = ptInput
      end
      item
        Name = 'WORKER_ID'
        ParamType = ptInput
      end
      item
        Name = 'CALC_PERIOD_ID'
        ParamType = ptInput
      end
      item
        Name = 'DATE_FROM'
        ParamType = ptInput
      end
      item
        Name = 'DATE_TO'
        ParamType = ptInput
      end
      item
        Name = 'ISSTORNO'
        ParamType = ptInput
      end>
  end
end
