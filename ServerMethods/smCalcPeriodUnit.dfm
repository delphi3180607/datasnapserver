inherited smCalcPeriod: TsmCalcPeriod
  inherited qrSelect: TFDQuery
    SQL.Strings = (
      'select * from v_calc_periods /*wherefilter*/ /*orderby*/;')
  end
  inherited qrDelete: TFDQuery
    SQL.Strings = (
      'delete from calc_periods where id = :id;')
    ParamData = <
      item
        Name = 'ID'
        ParamType = ptInput
        Value = Null
      end>
  end
  inherited qrUpdate: TFDQuery
    SQL.Strings = (
      'UPDATE public.calc_periods'
      'SET period_begin= :period_begin, date_closed = :date_closed'
      'WHERE id= :id;')
    ParamData = <
      item
        Name = 'PERIOD_BEGIN'
        ParamType = ptInput
      end
      item
        Name = 'DATE_CLOSED'
        ParamType = ptInput
      end
      item
        Name = 'ID'
        ParamType = ptInput
      end>
  end
  inherited qrInsert: TFDQuery
    SQL.Strings = (
      'INSERT INTO public.calc_periods (period_begin)'
      'values (:period_begin) returning id;')
    ParamData = <
      item
        Name = 'PERIOD_BEGIN'
        ParamType = ptInput
      end>
  end
end
