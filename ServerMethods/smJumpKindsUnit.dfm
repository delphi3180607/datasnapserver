inherited smJumpKinds: TsmJumpKinds
  inherited qrSelect: TFDQuery
    SQL.Strings = (
      'select * from v_jump_kinds /*wherefilter*/')
  end
  inherited qrDelete: TFDQuery
    SQL.Strings = (
      'delete from jump_kinds where id = :id;')
    ParamData = <
      item
        Name = 'ID'
        ParamType = ptInput
      end>
  end
  inherited qrUpdate: TFDQuery
    SQL.Strings = (
      'UPDATE public.jump_kinds'
      
        'SET payment_id= :payment_id, code= :code, "name"= :name, tarif= ' +
        ':tarif, "percent"= :percent, salary= :salary'
      'WHERE id= :id;')
    ParamData = <
      item
        Name = 'PAYMENT_ID'
        ParamType = ptInput
      end
      item
        Name = 'CODE'
        ParamType = ptInput
      end
      item
        Name = 'NAME'
        ParamType = ptInput
      end
      item
        Name = 'TARIF'
        ParamType = ptInput
      end
      item
        Name = 'PERCENT'
        ParamType = ptInput
      end
      item
        Name = 'SALARY'
        ParamType = ptInput
      end
      item
        Name = 'ID'
        ParamType = ptInput
      end>
  end
  inherited qrInsert: TFDQuery
    SQL.Strings = (
      'INSERT INTO public.jump_kinds'
      '(payment_id, code, "name", tarif, "percent", salary)'
      'VALUES(:payment_id, :code, :name, :tarif, :percent, :salary)'
      'returning id;')
    ParamData = <
      item
        Name = 'PAYMENT_ID'
        ParamType = ptInput
      end
      item
        Name = 'CODE'
        ParamType = ptInput
      end
      item
        Name = 'NAME'
        ParamType = ptInput
      end
      item
        Name = 'TARIF'
        ParamType = ptInput
      end
      item
        Name = 'PERCENT'
        ParamType = ptInput
      end
      item
        Name = 'SALARY'
        ParamType = ptInput
      end>
  end
end
