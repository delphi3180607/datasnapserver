inherited smReportColumns: TsmReportColumns
  inherited qrSelect: TFDQuery
    SQL.Strings = (
      'select * from v_report_columns /*wherefilter*/')
  end
  inherited qrDelete: TFDQuery
    SQL.Strings = (
      'delete from report_columns where id=:id;')
    ParamData = <
      item
        Name = 'ID'
        ParamType = ptInput
      end>
  end
  inherited qrUpdate: TFDQuery
    SQL.Strings = (
      'UPDATE public.report_columns'
      'SET report_id=:report_id, field_name=:field_name, title=:title, '
      'display_width=:display_width, display_align=:display_align, '
      'order_num = :order_num'
      'WHERE id=:id;')
    ParamData = <
      item
        Name = 'REPORT_ID'
        ParamType = ptInput
      end
      item
        Name = 'FIELD_NAME'
        ParamType = ptInput
      end
      item
        Name = 'TITLE'
        ParamType = ptInput
      end
      item
        Name = 'DISPLAY_WIDTH'
        ParamType = ptInput
      end
      item
        Name = 'DISPLAY_ALIGN'
        ParamType = ptInput
      end
      item
        Name = 'ORDER_NUM'
        ParamType = ptInput
      end
      item
        Name = 'ID'
        ParamType = ptInput
      end>
  end
  inherited qrInsert: TFDQuery
    SQL.Strings = (
      'INSERT INTO public.report_columns'
      
        '(report_id, field_name, title, display_width, display_align, ord' +
        'er_num)'
      
        'VALUES(:report_id, :field_name, :title, :display_width, :display' +
        '_align, :order_num)'
      'returning id;')
    ParamData = <
      item
        Name = 'REPORT_ID'
        ParamType = ptInput
      end
      item
        Name = 'FIELD_NAME'
        ParamType = ptInput
      end
      item
        Name = 'TITLE'
        ParamType = ptInput
      end
      item
        Name = 'DISPLAY_WIDTH'
        ParamType = ptInput
      end
      item
        Name = 'DISPLAY_ALIGN'
        ParamType = ptInput
      end
      item
        Name = 'ORDER_NUM'
        ParamType = ptInput
      end>
  end
end
