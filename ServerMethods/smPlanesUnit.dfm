inherited smPlanes: TsmPlanes
  inherited qrSelect: TFDQuery
    SQL.Strings = (
      'select * from v_planes /*wherefilter*/')
  end
  inherited qrDelete: TFDQuery
    SQL.Strings = (
      'delete from planes where id = :id;')
    ParamData = <
      item
        Name = 'ID'
        ParamType = ptInput
      end>
  end
  inherited qrUpdate: TFDQuery
    SQL.Strings = (
      'UPDATE public.planes'
      'SET code= :code, "name"= :name'
      'WHERE id= :id;')
    ParamData = <
      item
        Name = 'CODE'
        ParamType = ptInput
      end
      item
        Name = 'NAME'
        ParamType = ptInput
      end
      item
        Name = 'ID'
        ParamType = ptInput
      end>
  end
  inherited qrInsert: TFDQuery
    SQL.Strings = (
      'INSERT INTO public.planes (code, "name")'
      'VALUES(:code, :name) returning id;')
    ParamData = <
      item
        Name = 'CODE'
        ParamType = ptInput
      end
      item
        Name = 'NAME'
        ParamType = ptInput
      end>
  end
end
