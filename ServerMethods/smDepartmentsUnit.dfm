inherited smDepartments: TsmDepartments
  inherited qrSelect: TFDQuery
    SQL.Strings = (
      'select * from v_departments /*wherefilter*/')
  end
  object qrExtended: TFDQuery
    Connection = dmConnection.connLocal
    SQL.Strings = (
      'select d.*,'
      'd.code||'
      'coalesce ((select '#39'(+)'#39' where exists '
      '  ('
      '    select 1 '
      '    from accurals a, workers w'
      '    where a.worker_id = w.id'
      '    and a.calc_period_id = :calc_period_id'
      '    and w.department_id = d.id  '
      '  ) '
      '),'#39#39') as code_marked  '
      'from v_departments d /*wherefilter*/'
      '')
    Left = 208
    Top = 120
    ParamData = <
      item
        Name = 'CALC_PERIOD_ID'
        ParamType = ptInput
      end>
  end
end
