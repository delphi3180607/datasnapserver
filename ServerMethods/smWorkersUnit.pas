unit smWorkersUnit;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, smBaseUnit, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  FMAParamTransform, Data.DB, FireDAC.Comp.DataSet, FireDAC.Comp.Client, JSON;

{$METHODINFO ON}
type
  TsmWorkers = class(TsmBase)
    qrExtended: TFDQuery;
  private
    { Private declarations }
  public
    function GetListExtended(filter: TJSONObject; out result_state: boolean; out errcode: string; out errmessage: string): TDataSet;
  end;
{$METHODINFO OFF}

var
  smWorkers: TsmWorkers;

implementation

{$R *.dfm}

{ TsmWorkers }

function TsmWorkers.GetListExtended(filter: TJSONObject; out result_state: boolean; out errcode, errmessage: string): TDataSet;
begin
  ApplyContext(filter, qrExtended);
  if QueryOpen(qrExtended, errcode, errmessage) then
  begin
    result_state := true;
    result := qrExtended;
  end else
  begin
    result_state := false;
    result := nil;
  end;
end;

end.
