inherited smCommonPays: TsmCommonPays
  inherited qrSelect: TFDQuery
    SQL.Strings = (
      'select * from v_common_pays /*wherefilter*/')
  end
  inherited qrDelete: TFDQuery
    SQL.Strings = (
      'delete from common_pays where id = :id;')
    ParamData = <
      item
        Name = 'ID'
        ParamType = ptInput
      end>
  end
  inherited qrUpdate: TFDQuery
    SQL.Strings = (
      'UPDATE public.common_pays'
      
        'SET worker_id= :worker_id, payment_id=:payment_id, calc_period_i' +
        'd=:calc_period_id, "percent"=:percent, summa=:summa'
      'WHERE id=:id;')
    ParamData = <
      item
        Name = 'WORKER_ID'
        ParamType = ptInput
      end
      item
        Name = 'PAYMENT_ID'
        ParamType = ptInput
      end
      item
        Name = 'CALC_PERIOD_ID'
        ParamType = ptInput
      end
      item
        Name = 'PERCENT'
        ParamType = ptInput
      end
      item
        Name = 'SUMMA'
        ParamType = ptInput
      end
      item
        Name = 'ID'
        ParamType = ptInput
      end>
  end
  inherited qrInsert: TFDQuery
    SQL.Strings = (
      'INSERT INTO public.common_pays'
      '(worker_id, payment_id, calc_period_id, "percent", summa)'
      
        'VALUES(:worker_id, :payment_id, :calc_period_id, :percent, :summ' +
        'a)'
      ' returning id;')
    ParamData = <
      item
        Name = 'WORKER_ID'
        ParamType = ptInput
      end
      item
        Name = 'PAYMENT_ID'
        ParamType = ptInput
      end
      item
        Name = 'CALC_PERIOD_ID'
        ParamType = ptInput
      end
      item
        Name = 'PERCENT'
        ParamType = ptInput
      end
      item
        Name = 'SUMMA'
        ParamType = ptInput
      end>
  end
end
