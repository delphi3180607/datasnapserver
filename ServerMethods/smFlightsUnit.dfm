inherited smFlights: TsmFlights
  inherited qrSelect: TFDQuery
    SQL.Strings = (
      'select * from v_flights /*wherefilter*/')
  end
  inherited qrDelete: TFDQuery
    SQL.Strings = (
      'delete from flights where id = :id;')
    ParamData = <
      item
        Name = 'ID'
        ParamType = ptInput
      end>
  end
  inherited qrUpdate: TFDQuery
    SQL.Strings = (
      'UPDATE public.flights'
      
        'SET fire_case_id= :fire_case_id, worker_id= :worker_id, plane_id' +
        '= :plane_id, '
      'pay_group_id= :pay_group_id, calc_period_id= :calc_period_id, '
      'date_from= :date_from, date_to= :date_to, hours= :hours, '
      'isstorno= :isstorno'
      'WHERE id= :id;')
    ParamData = <
      item
        Name = 'FIRE_CASE_ID'
        ParamType = ptInput
      end
      item
        Name = 'WORKER_ID'
        ParamType = ptInput
      end
      item
        Name = 'PLANE_ID'
        ParamType = ptInput
      end
      item
        Name = 'PAY_GROUP_ID'
        ParamType = ptInput
      end
      item
        Name = 'CALC_PERIOD_ID'
        ParamType = ptInput
      end
      item
        Name = 'DATE_FROM'
        ParamType = ptInput
      end
      item
        Name = 'DATE_TO'
        ParamType = ptInput
      end
      item
        Name = 'HOURS'
        ParamType = ptInput
      end
      item
        Name = 'ISSTORNO'
        ParamType = ptInput
      end
      item
        Name = 'ID'
        ParamType = ptInput
      end>
  end
  inherited qrInsert: TFDQuery
    SQL.Strings = (
      'INSERT INTO public.flights'
      
        '(fire_case_id, worker_id, plane_id, pay_group_id, calc_period_id' +
        ', date_from, date_to, hours, isstorno)'
      
        'VALUES(:fire_case_id, :worker_id, :plane_id, :pay_group_id, :cal' +
        'c_period_id, :date_from, :date_to, :hours, :isstorno)'
      'returning id;')
    ParamData = <
      item
        Name = 'FIRE_CASE_ID'
        ParamType = ptInput
      end
      item
        Name = 'WORKER_ID'
        ParamType = ptInput
      end
      item
        Name = 'PLANE_ID'
        ParamType = ptInput
      end
      item
        Name = 'PAY_GROUP_ID'
        ParamType = ptInput
      end
      item
        Name = 'CALC_PERIOD_ID'
        ParamType = ptInput
      end
      item
        Name = 'DATE_FROM'
        ParamType = ptInput
      end
      item
        Name = 'DATE_TO'
        ParamType = ptInput
      end
      item
        Name = 'HOURS'
        ParamType = ptInput
      end
      item
        Name = 'ISSTORNO'
        ParamType = ptInput
      end>
  end
end
