inherited smFireCases: TsmFireCases
  inherited qrSelect: TFDQuery
    SQL.Strings = (
      'select * from v_fire_cases /*wherefilter*/;')
  end
  inherited qrDelete: TFDQuery
    SQL.Strings = (
      'delete from fire_cases where id = :id;')
    ParamData = <
      item
        Name = 'ID'
        ParamType = ptInput
      end>
  end
  inherited qrUpdate: TFDQuery
    SQL.Strings = (
      'UPDATE public.fire_cases'
      
        'SET "name"= :name, forestry_id= :forestry_id, square= :square, d' +
        'epartment_id= :department_id, datefrom= :datefrom, dateto= :date' +
        'to'
      'WHERE id= :id;')
    ParamData = <
      item
        Name = 'NAME'
        ParamType = ptInput
      end
      item
        Name = 'FORESTRY_ID'
        ParamType = ptInput
      end
      item
        Name = 'SQUARE'
        ParamType = ptInput
      end
      item
        Name = 'DEPARTMENT_ID'
        ParamType = ptInput
      end
      item
        Name = 'DATEFROM'
        ParamType = ptInput
      end
      item
        Name = 'DATETO'
        ParamType = ptInput
      end
      item
        Name = 'ID'
        ParamType = ptInput
      end>
  end
  inherited qrInsert: TFDQuery
    SQL.Strings = (
      'INSERT INTO public.fire_cases'
      '("name", forestry_id, square, department_id, datefrom, dateto)'
      
        'VALUES(:name, :forestry_id, :square, :department_id, :datefrom, ' +
        ':dateto)'
      'returning id;')
    ParamData = <
      item
        Name = 'NAME'
        ParamType = ptInput
      end
      item
        Name = 'FORESTRY_ID'
        ParamType = ptInput
      end
      item
        Name = 'SQUARE'
        ParamType = ptInput
      end
      item
        Name = 'DEPARTMENT_ID'
        ParamType = ptInput
      end
      item
        Name = 'DATEFROM'
        ParamType = ptInput
      end
      item
        Name = 'DATETO'
        ParamType = ptInput
      end>
  end
  object qrSelectEx: TFDQuery
    Connection = dmConnection.connLocal
    SQL.Strings = (
      'select c.*,'
      'coalesce ( '
      '('
      '   select 1  '
      '   where exists ('
      '    select 1 from fightings f'
      '    where f.calc_period_id = :calc_period_id'
      '    and f.fire_case_id = c.id'
      '    and f.isstorno '
      '   )'
      '   or exists ('
      '    select 1 from jumps f'
      '    where f.calc_period_id = :calc_period_id'
      '    and f.fire_case_id = c.id '
      '    and f.isstorno '
      '   )'
      '   or exists ('
      '    select 1 from other_pays f'
      '    where f.calc_period_id = :calc_period_id'
      '    and f.fire_case_id = c.id '
      '    and f.isstorno '
      '   )'
      '   or exists ('
      '    select 1 from flights f'
      '    where f.calc_period_id = :calc_period_id'
      '    and f.fire_case_id = c.id '
      '    and f.isstorno '
      '   )'
      '),0) as storno_mark  '
      'from v_fire_cases c '
      'where '
      
        '((c.datefrom <= :date_end) and (c.dateto >= :date_begin or c.dat' +
        'eto is null) ) '
      'or exists ('
      ' select 1 from fightings f'
      ' where f.calc_period_id = :calc_period_id'
      ' and f.fire_case_id = c.id '
      ')'
      'or exists ('
      ' select 1 from jumps f'
      ' where f.calc_period_id = :calc_period_id'
      ' and f.fire_case_id = c.id '
      ')'
      'or exists ('
      ' select 1 from other_pays f'
      ' where f.calc_period_id = :calc_period_id'
      ' and f.fire_case_id = c.id '
      ')'
      'or exists ('
      ' select 1 from flights f'
      ' where f.calc_period_id = :calc_period_id'
      ' and f.fire_case_id = c.id '
      ')'
      '')
    Left = 192
    Top = 120
    ParamData = <
      item
        Name = 'CALC_PERIOD_ID'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end
      item
        Name = 'DATE_END'
        DataType = ftDate
        ParamType = ptInput
        Value = Null
      end
      item
        Name = 'DATE_BEGIN'
        DataType = ftDate
        ParamType = ptInput
        Value = Null
      end>
  end
end
