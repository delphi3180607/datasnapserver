inherited smFlightTarifs: TsmFlightTarifs
  inherited qrSelect: TFDQuery
    SQL.Strings = (
      'select * from v_flight_tarifs /*wherefilter*/')
  end
  inherited qrDelete: TFDQuery
    SQL.Strings = (
      'delete from flight_tarifs where id = :id;')
    ParamData = <
      item
        Name = 'ID'
        ParamType = ptInput
      end>
  end
  inherited qrUpdate: TFDQuery
    SQL.Strings = (
      'UPDATE public.flight_tarifs'
      
        'SET plane_id= :plane_id, paygroup_id= :paygroup_id, head_salary=' +
        ' :head_salary, "percent"= :percent, tarif= :tarif'
      'WHERE id= :id;')
    ParamData = <
      item
        Name = 'PLANE_ID'
        ParamType = ptInput
      end
      item
        Name = 'PAYGROUP_ID'
        ParamType = ptInput
      end
      item
        Name = 'HEAD_SALARY'
        ParamType = ptInput
      end
      item
        Name = 'PERCENT'
        ParamType = ptInput
      end
      item
        Name = 'TARIF'
        ParamType = ptInput
      end
      item
        Name = 'ID'
        ParamType = ptInput
      end>
  end
  inherited qrInsert: TFDQuery
    SQL.Strings = (
      'INSERT INTO public.flight_tarifs'
      '(plane_id, paygroup_id, head_salary, "percent", tarif)'
      'VALUES(:plane_id, :paygroup_id, :head_salary, :percent, :tarif)'
      'returning id;')
    ParamData = <
      item
        Name = 'PLANE_ID'
        ParamType = ptInput
      end
      item
        Name = 'PAYGROUP_ID'
        ParamType = ptInput
      end
      item
        Name = 'HEAD_SALARY'
        ParamType = ptInput
      end
      item
        Name = 'PERCENT'
        ParamType = ptInput
      end
      item
        Name = 'TARIF'
        ParamType = ptInput
      end>
  end
end
