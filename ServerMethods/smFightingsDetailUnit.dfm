inherited smFightingsDetail: TsmFightingsDetail
  inherited qrSelect: TFDQuery
    SQL.Strings = (
      'select * from v_fighting_details /*wherefilter*/')
  end
  inherited qrDelete: TFDQuery
    SQL.Strings = (
      'delete from fighting_details where id = :id;')
    ParamData = <
      item
        Name = 'ID'
        ParamType = ptInput
      end>
  end
  inherited qrUpdate: TFDQuery
    SQL.Strings = (
      'UPDATE public.fighting_details'
      
        'SET fighting_id= :fighting_id, "day"= :day, work_hours= :work_ho' +
        'urs, overtime_hours= :overtime_hours, holiday_hours= :holiday_ho' +
        'urs'
      'WHERE id= :id;')
    ParamData = <
      item
        Name = 'FIGHTING_ID'
        ParamType = ptInput
      end
      item
        Name = 'DAY'
        ParamType = ptInput
      end
      item
        Name = 'WORK_HOURS'
        ParamType = ptInput
      end
      item
        Name = 'OVERTIME_HOURS'
        ParamType = ptInput
      end
      item
        Name = 'HOLIDAY_HOURS'
        ParamType = ptInput
      end
      item
        Name = 'ID'
        ParamType = ptInput
      end>
  end
  inherited qrInsert: TFDQuery
    SQL.Strings = (
      'INSERT INTO fighting_details'
      '(fighting_id, "day", work_hours, overtime_hours, holiday_hours)'
      
        'VALUES(:fighting_id, :day, :work_hours, :overtime_hours, :holida' +
        'y_hours)'
      'returning id;')
    ParamData = <
      item
        Name = 'FIGHTING_ID'
        ParamType = ptInput
      end
      item
        Name = 'DAY'
        ParamType = ptInput
      end
      item
        Name = 'WORK_HOURS'
        ParamType = ptInput
      end
      item
        Name = 'OVERTIME_HOURS'
        ParamType = ptInput
      end
      item
        Name = 'HOLIDAY_HOURS'
        ParamType = ptInput
      end>
  end
end
