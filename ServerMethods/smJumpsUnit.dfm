inherited smJumps: TsmJumps
  inherited qrSelect: TFDQuery
    SQL.Strings = (
      'select * from v_jumps /*wherefilter*/')
  end
  inherited qrDelete: TFDQuery
    SQL.Strings = (
      'delete from jumps where id = :id;')
    ParamData = <
      item
        Name = 'ID'
        ParamType = ptInput
      end>
  end
  inherited qrUpdate: TFDQuery
    SQL.Strings = (
      'UPDATE public.jumps'
      
        'SET fire_case_id= :fire_case_id, worker_id= :worker_id, jump_kin' +
        'd_id= :jump_kind_id, amount= :amount, '
      
        'calc_period_id= :calc_period_id, date_begin= :date_begin, date_e' +
        'nd= :date_end, isstorno = :isstorno'
      'WHERE id= :id;')
    ParamData = <
      item
        Name = 'FIRE_CASE_ID'
        ParamType = ptInput
      end
      item
        Name = 'WORKER_ID'
        ParamType = ptInput
      end
      item
        Name = 'JUMP_KIND_ID'
        ParamType = ptInput
      end
      item
        Name = 'AMOUNT'
        ParamType = ptInput
      end
      item
        Name = 'CALC_PERIOD_ID'
        ParamType = ptInput
      end
      item
        Name = 'DATE_BEGIN'
        ParamType = ptInput
      end
      item
        Name = 'DATE_END'
        ParamType = ptInput
      end
      item
        Name = 'ISSTORNO'
        ParamType = ptInput
      end
      item
        Name = 'ID'
        ParamType = ptInput
      end>
  end
  inherited qrInsert: TFDQuery
    SQL.Strings = (
      'INSERT INTO public.jumps'
      
        '(fire_case_id, worker_id, jump_kind_id, amount, calc_period_id, ' +
        'date_begin, date_end, isstorno)'
      
        'VALUES(:fire_case_id, :worker_id, :jump_kind_id, :amount, :calc_' +
        'period_id, :date_begin, :date_end, :isstorno)'
      'returning id;')
    ParamData = <
      item
        Name = 'FIRE_CASE_ID'
        ParamType = ptInput
      end
      item
        Name = 'WORKER_ID'
        ParamType = ptInput
      end
      item
        Name = 'JUMP_KIND_ID'
        ParamType = ptInput
      end
      item
        Name = 'AMOUNT'
        ParamType = ptInput
      end
      item
        Name = 'CALC_PERIOD_ID'
        ParamType = ptInput
      end
      item
        Name = 'DATE_BEGIN'
        ParamType = ptInput
      end
      item
        Name = 'DATE_END'
        ParamType = ptInput
      end
      item
        Name = 'ISSTORNO'
        ParamType = ptInput
      end>
  end
end
