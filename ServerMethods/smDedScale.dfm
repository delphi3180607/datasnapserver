inherited smBase1: TsmBase1
  inherited qrSelect: TFDQuery
    SQL.Strings = (
      
        'select * from deduction_scale /*wherefilter*/ order by start_dat' +
        'e;')
  end
  inherited qrDelete: TFDQuery
    SQL.Strings = (
      'delete from deduction_scale where id = :id;')
    ParamData = <
      item
        Name = 'ID'
        ParamType = ptInput
      end>
  end
  inherited qrUpdate: TFDQuery
    SQL.Strings = (
      'UPDATE public.deduction_scale'
      'SET start_date= :start_date, esv= :esv, pfr= :pfr, fss= :fss'
      'WHERE id= :id;')
    ParamData = <
      item
        Name = 'START_DATE'
        ParamType = ptInput
      end
      item
        Name = 'ESV'
        ParamType = ptInput
      end
      item
        Name = 'PFR'
        ParamType = ptInput
      end
      item
        Name = 'FSS'
        ParamType = ptInput
      end
      item
        Name = 'ID'
        ParamType = ptInput
      end>
  end
  inherited qrInsert: TFDQuery
    SQL.Strings = (
      'INSERT INTO public.deduction_scale'
      '(start_date, esv, pfr, fss)'
      'VALUES(:start_date, :esv, :pfr, :fss);')
    ParamData = <
      item
        Name = 'START_DATE'
        ParamType = ptInput
      end
      item
        Name = 'ESV'
        ParamType = ptInput
      end
      item
        Name = 'PFR'
        ParamType = ptInput
      end
      item
        Name = 'FSS'
        ParamType = ptInput
      end>
  end
end
