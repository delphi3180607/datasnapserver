unit Connection;

interface

uses
  System.SysUtils, System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.ODBC,
  FireDAC.Phys.ODBCDef, FireDAC.VCLUI.Wait, Data.DB, FireDAC.Comp.Client,
  IniFiles, StrUtils;

type
  TdmConnection = class(TDataModule)
    connLocal: TFDConnection;
  private
    DriverID: string;
    DataSource: string;
    Password: string;
    UserName: string;
    Port: string;
    function IndexOfParam(param_name: string): integer;
    procedure ReadConfig;
    procedure SetConnectionParam(param_name, param_value: string);
  public
    function Connect: boolean;
  end;

var
  dmConnection: TdmConnection;

implementation

uses LoggerUnit, ServerContainerUnit;

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

function TdmConnection.Connect: boolean;
begin
   result := true;
   ReadConfig;
   try
    connLocal.Connected := true;
   except
    if not connLocal.Connected then
    begin
      Logger.Add('�� ������� ������������ � ���� ������.');
      result := false;
    end;
   end;
end;

function TdmConnection.IndexOfParam(param_name: string): integer;
var i: integer;
begin
  result := -1;
  for i := 0 to connLocal.Params.Count-1 do
  begin
    if pos(param_name,connLocal.Params[i])=0 then
    begin
      result := i;
      exit;
    end;
  end;
end;

procedure TdmConnection.ReadConfig;
var AppDir: string; infconn: TIniFile;
begin
  AppDir := ExtractFilePath(GetModuleName(HInstance));
  infconn := TIniFile.Create(AppDir+'/config.ini');

  DriverID := infconn.ReadString('main','DriverID','');
  DataSource := infconn.ReadString('main','DataSource','');
  Password := infconn.ReadString('main','Password','');
  UserName := infconn.ReadString('main','UserName','');
  Port := infconn.ReadString('main','Port','80');

  connLocal.Params.Clear;

  SetConnectionParam('DriverId', DriverID);
  SetConnectionParam('DataSource', DataSource);
  SetConnectionParam('Password', Password);
  SetConnectionParam('UserName', UserName);

  try
    AviaWagesServer.DSHTTPSrv.HttpPort := StrToInt(Port);
  except
    begin
      AviaWagesServer.DSHTTPSrv.HttpPort := 80;
      Logger.Add('�� ������� ��������� ����� ����� �� ������������. ���������� ���� 80 �� ���������.');
    end;
  end;

end;


procedure TdmConnection.SetConnectionParam(param_name, param_value: string);
var index: integer;
begin
  index := connLocal.Params.Add(param_name);
  connLocal.Params[index] := param_name+'='+param_value;
end;


end.
