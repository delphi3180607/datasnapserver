unit LoggerUnit;

interface

uses
  System.SysUtils, System.Classes, uLog;

type
  TLogger = class(TDataModule)
    procedure DataModuleCreate(Sender: TObject);
  private
    FileName: string;
  public
    procedure Add(info: string);
  end;

var
  Logger: TLogger;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

{ TLogger }

procedure TLogger.Add(info: string);
begin
  sLog(FileName, info);
end;

procedure TLogger.DataModuleCreate(Sender: TObject);
begin
  FileName := ExtractFilePath(GetModuleName(HInstance))+'Log.txt';
end;

end.
