program DataSnapServer;

uses
  Vcl.SvcMgr,
  Web.WebReq,
  Dialogs,
  IdHTTPWebBrokerBridge,
  main in 'main.pas' {FormMain},
  ServerContainerUnit in 'ServerContainerUnit.pas' {AviaWagesServer: TDataModule},
  Connection in 'Connection.pas' {dmConnection: TDataModule},
  smBaseUnit in 'ServerMethods\smBaseUnit.pas' {smBase},
  smCalcPeriodUnit in 'ServerMethods\smCalcPeriodUnit.pas' {smCalcPeriod: TDataModule},
  smCommonPaysUnit in 'ServerMethods\smCommonPaysUnit.pas' {smCommonPays: TDataModule},
  smFightingsUnit in 'ServerMethods\smFightingsUnit.pas' {smFightings: TDataModule},
  smFireCasesUnit in 'ServerMethods\smFireCasesUnit.pas' {smFireCases: TDataModule},
  smFlightsUnit in 'ServerMethods\smFlightsUnit.pas' {smFlights: TDataModule},
  smJumpsUnit in 'ServerMethods\smJumpsUnit.pas' {smJumps: TDataModule},
  smOtherPaysUnit in 'ServerMethods\smOtherPaysUnit.pas' {smOtherPays: TDataModule},
  smForestriesUnit in 'ServerMethods\smForestriesUnit.pas' {smForestries: TDataModule},
  smJumpKindsUnit in 'ServerMethods\smJumpKindsUnit.pas' {smJumpKinds: TDataModule},
  smPlanesUnit in 'ServerMethods\smPlanesUnit.pas' {smPlanes: TDataModule},
  smPayGroupsUnit in 'ServerMethods\smPayGroupsUnit.pas' {smPayGroups: TDataModule},
  smFlightTarifsUnit in 'ServerMethods\smFlightTarifsUnit.pas' {smFlightTarifs: TDataModule},
  smFightingsDetailUnit in 'ServerMethods\smFightingsDetailUnit.pas' {smFightingsDetail: TDataModule},
  smDepartmentsUnit in 'ServerMethods\smDepartmentsUnit.pas' {smDepartments: TDataModule},
  smWorkersUnit in 'ServerMethods\smWorkersUnit.pas' {smWorkers: TDataModule},
  smGroundsUnit in 'ServerMethods\smGroundsUnit.pas' {smGrounds: TDataModule},
  smPaymentsUnit in 'ServerMethods\smPaymentsUnit.pas' {smPayments: TDataModule},
  smCatalogsUnit in 'ServerMethods\smCatalogsUnit.pas' {smCatalogs: TDataModule},
  smPayLinksUnit in 'ServerMethods\smPayLinksUnit.pas' {smPayLinks: TDataModule},
  smPayments2PaymentsUnit in 'ServerMethods\smPayments2PaymentsUnit.pas' {smPayments2Payments: TDataModule},
  LoggerUnit in 'Utils\LoggerUnit.pas' {Logger: TDataModule},
  uLog in 'Utils\uLog.pas',
  smUtilsUnit in 'ServerMethods\smUtilsUnit.pas' {smUtils: TDataModule},
  smAccuralsUnit in 'ServerMethods\smAccuralsUnit.pas' {smAccurals: TDataModule},
  smFOTUnit in 'ServerMethods\smFOTUnit.pas' {smFOT: TDataModule},
  smReportsUnit in 'ServerMethods\smReportsUnit.pas' {smReports: TDataModule},
  smReportColumnsUnit in 'ServerMethods\smReportColumnsUnit.pas' {smReportColumns: TDataModule},
  smReportParamsUnit in 'ServerMethods\smReportParamsUnit.pas' {smReportParams: TDataModule},
  smDedScaleUnit in 'ServerMethods\smDedScaleUnit.pas' {smDedScale: TDataModule};

{$R *.res}

begin

  Application.CreateForm(TdmConnection, dmConnection);
  Application.CreateForm(TFormMain, FormMain);
  Application.CreateForm(TsmCalcPeriod, smCalcPeriod);
  Application.CreateForm(TsmCommonPays, smCommonPays);
  Application.CreateForm(TsmFightings, smFightings);
  Application.CreateForm(TsmFireCases, smFireCases);
  Application.CreateForm(TsmFlights, smFlights);
  Application.CreateForm(TsmJumps, smJumps);
  Application.CreateForm(TsmOtherPays, smOtherPays);
  Application.CreateForm(TsmForestries, smForestries);
  Application.CreateForm(TsmJumpKinds, smJumpKinds);
  Application.CreateForm(TsmPlanes, smPlanes);
  Application.CreateForm(TsmPayGroups, smPayGroups);
  Application.CreateForm(TsmFlightTarifs, smFlightTarifs);
  Application.CreateForm(TsmFightingsDetail, smFightingsDetail);
  Application.CreateForm(TsmDepartments, smDepartments);
  Application.CreateForm(TsmWorkers, smWorkers);
  Application.CreateForm(TsmGrounds, smGrounds);
  Application.CreateForm(TsmPayments, smPayments);
  Application.CreateForm(TsmCatalogs, smCatalogs);
  Application.CreateForm(TsmPayLinks, smPayLinks);
  Application.CreateForm(TsmPayments2Payments, smPayments2Payments);
  Application.CreateForm(TLogger, Logger);
  Application.CreateForm(TsmUtils, smUtils);
  Application.CreateForm(TsmAccurals, smAccurals);
  Application.CreateForm(TsmFOT, smFOT);
  Application.CreateForm(TsmReports, smReports);
  Application.CreateForm(TsmReportColumns, smReportColumns);
  Application.CreateForm(TsmReportParams, smReportParams);
  Application.CreateForm(TsmDedScale, smDedScale);
  if Paramstr(1)='-runassvc' then
  begin
    if not Application.DelayInitialize or Application.Installing then
      Application.Initialize;
    Application.CreateForm(TAviaWagesServer, AviaWagesServer);
  end else
  begin
    Application.CreateForm(TAviaWagesServer, AviaWagesServer);
    if dmConnection.Connect then
    begin
      AviaWagesServer.DSAppServer.Start;
    end else
    begin
      AviaWagesServer.DSAppServer.Stop;
    end;
    FormMain.ShowModal;
  end;
  Application.Run;

end.

